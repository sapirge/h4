import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],

})

export class MoviesComponent implements OnInit {
  
  name = "no movie";
  displayedColumns: string[] = ['id', 'title', 'studio', 'weekendIncom', 'delete'];

  movies = [];
 

  toDelete(element)
  {

    let start, end = this.movies;
    let id = element.id;
    let i = 0;
    let deleted = [];
      
    for(let movie of this.movies)
    {
      deleted[i] = movie.id;
      i++;
    }
    
    start = this.movies.slice(0,deleted.indexOf(id));
    end = this.movies.slice(deleted.indexOf(id)+1, this.movies.length+1);
    this.movies = start;
    this.movies = this.movies.concat(end);   
    this.name = element.title;

  }

 
  constructor(private db: AngularFireDatabase) { }


  ngOnInit() {
    this.db.list('/movies').snapshotChanges().subscribe(
      movies => {
        this.movies = [];
        movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            //y['$key'] = movie.key;
            this.movies.push(y);
          }
        )
      }
    )
  }

 
}
